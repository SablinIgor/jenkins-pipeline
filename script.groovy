def buildApp() {
    echo 'Building application...'
}

def testApp() {
    echo 'Testing application...'
}

def deployApp() {
    echo 'Deploying application...'
}

return this